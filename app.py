from flask import Flask, request, render_template, redirect
from flask_sqlalchemy import SQLAlchemy
from uuid import uuid4


app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql+psycopg2://:@nuts3.test/mock-cas"

db = SQLAlchemy(app)


@app.route('/cas/login')
def login():
    return render_template('login.html', service=request.args.get('service'))


@app.route('/cas/login/submit', methods=['POST'])
def submit():
    print(request.args)
    st = 'ST-' + uuid4().hex
    t = Token(user_name=request.form.get('username'), service_token=st)
    db.session.add(t)
    db.session.commit()
    print(request.form.get('username'))
    print(request.form.get('service'))
    return redirect(request.form.get('service') + '&ticket=' + st, 307)


@app.route('/cas/validate')
def validate():
    ticket = request.args.get('ticket')
    t = Token.query.filter_by(service_token=ticket).first()
    return render_template('validate.html', username=t.user_name)


class Token(db.Model):
    __tablename__ = 'tokens'
    id = db.Column(db.Integer, primary_key=True)
    service_token = db.Column(db.String(50))
    user_name = db.Column(db.String(50))
